/* Задание на урок:

1) Первую часть задания повторить по уроку

2) Создать функцию showMyDB, которая будет проверять свойство privat. Если стоит в позиции
false - выводит в консоль главный объект программы

3) Создать функцию writeYourGenres в которой пользователь будет 3 раза отвечать на вопрос 
"Ваш любимый жанр под номером ${номер по порядку}". Каждый ответ записывается в массив данных
genres

P.S. Функции вызывать не обязательно*/

'use strict';

// Код возьмите из предыдущего домашнего задания



let numberOfFilms = null;
do {
    numberOfFilms = +prompt("Сколько фильмов вы уже посмотрели?", "");
    if (isNaN(numberOfFilms)) {
        alert("Введенный ответ не цифа. Пожалуйста, повторите ввод.");
        console.log("isNaN(numberOfFilms): " + isNaN(numberOfFilms));
        numberOfFilms = null;
    }
} while (!numberOfFilms);

let personalMovieDB = {
    "count": numberOfFilms,
    "movies": {},
    "actors": {},
    "genres": [],
    "privat": false
};

let i = 0;
while (i < 2) {
    let filmName = prompt("Один из последних просмотренных фильмов?", "");
    while (filmName === null || filmName.trim() == "" || filmName.trim().length > 50) {
        let message = "";
        if (filmName === null) {
            message = "Название фильма обязательно для заполнения.";
        } else if (filmName.trim() == "") {
            message = "Название фильма не может быть пустой строкой.";
        } else {
            message = "Название фильма не может быть длинне 50 символов.";
        }
        filmName = prompt(`${message} Пожалуйста, введите название одного из последних просмотренных фильмов?`, "");
    }
    console.log("filmName: " + filmName);
    let lastWatchedFilmRank = null;
    do {
        lastWatchedFilmRank = +prompt("На сколько оцените его?", "");
        if (isNaN(lastWatchedFilmRank)) {
            alert("Введенный ответ не цифа. Пожалуйста, повторите ввод.");
            console.log("isNaN(lastWatchedFilmRank): " + isNaN(lastWatchedFilmRank));
            lastWatchedFilmRank = null;
        }
    } while (!lastWatchedFilmRank);
    console.log("lastWatchedFilmRank: " + lastWatchedFilmRank);
    personalMovieDB.movies[filmName] = lastWatchedFilmRank;
    i++;
}

let message = "";
switch (personalMovieDB.count) {
    case ((personalMovieDB.count > 0 && personalMovieDB.count < 10) ? personalMovieDB.count : !personalMovieDB.count ): 
        message = "Просмотрено довольно мало фильмов";
        alert(message);
        console.log(message);
        break;
    case ((personalMovieDB.count > 9 && personalMovieDB.count < 30) ? personalMovieDB.count : !personalMovieDB.count ):
        message = "Вы классический зритель";
        alert(message);
        console.log(message);
        break;
    case ((personalMovieDB.count > 29) ? personalMovieDB.count : !personalMovieDB.count ):
        message = "Вы киноман";
        alert(message);
        console.log(message);
        break;
    default:
        message = "Произошла ошибка";
        alert(message);
        console.log(message);
        break;
};

ShowMyDB(personalMovieDB);

for(let i = 1; i < 4; i++){
    personalMovieDB.genres.push(WriteYourGenres(i));
}


function WriteYourGenres(position){
    let genre = null;
    do{
        genre = prompt(`Ваш любимый жанр под номером ${position}`, "");
        if(genre == null) { 
            let message = "Жанр - обязателен к заполнению.";
            alert(message);
            console.log(message);
        } else if( genre.trim() == ""){
            let message = "Жанр - не может быть пустой строкой.";
            alert(message);
            console.log(message);
            genre = null;
        }
    } while (!genre);

    return genre;
}

function ShowMyDB(movieDb){
    if(!movieDb.privat){
        console.log(movieDb);
    }
}