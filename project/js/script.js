/* Задания на урок:

1) Реализовать функционал, что после заполнения формы и нажатия кнопки "Подтвердить" - 
новый фильм добавляется в список. Страница не должна перезагружаться.
Новый фильм должен добавляться в movieDB.movies.
Для получения доступа к значению input - обращаемся к нему как input.value;
P.S. Здесь есть несколько вариантов решения задачи, принимается любой, но рабочий.

2) Если название фильма больше, чем 21 символ - обрезать его и добавить три точки

3) При клике на мусорную корзину - элемент будет удаляться из списка (сложно)

4) Если в форме стоит галочка "Сделать любимым" - в консоль вывести сообщение: 
"Добавляем любимый фильм"

5) Фильмы должны быть отсортированы по алфавиту */

'use strict';

// Возьмите свой код из предыдущей практики

document.addEventListener("DOMContentLoaded", () => {
    const movieDB = {
        movies: [
            "Логан",
            "Лига справедливости",
            "Ла-ла лэнд",
            "Одержимость",
            "Скотт Пилигрим против..."
        ]
    };





    // solution 2
    var addForm = document.querySelector("form.add");
    var addInput = addForm.querySelector(".adding__input");
    var checkbox = addForm.querySelector('[type="checkbox"]'); //[] - для обозначения атрибута 
    var filmsList = document.querySelector(".promo__interactive-list");
    RebuildFilmsList(movieDB.movies, filmsList);

    addForm.addEventListener("submit", (event) => {
        event.preventDefault();

        let isCheckedFavourite = checkbox.checked;
        let filmName = addInput.value;

        if (filmName) {
            if (isCheckedFavourite) {
                console.log(`Добавляем любимый фильм: ${filmName}`);
            }
            movieDB.movies.push(filmName);
            RebuildFilmsList(movieDB.movies, filmsList);
        }

        //reset values
        // my version
        // addInput.value = "";
        // checkbox.checked = false;
        
        //prod v 1
        // addForm.reset();
        //prod v 2
        event.target.reset();
    });

    function RebuildFilmsList(films, parent) {
        const maxFilmLengthShow = 21;
        films.sort();

        //console.dir(filmsList);
        parent.innerHTML = "";
        films.forEach((item, i) => {
            parent.innerHTML += `
    <li class="promo__interactive-item">${i + 1} ${item.length > maxFilmLengthShow ? item.substring(0, maxFilmLengthShow) + "..." : item}
        <div class="delete"></div>
    </li>
`;
        });

        document.querySelectorAll(".delete").forEach((btn, i) => {
            btn.addEventListener("click", () => {
                btn.parentElement.remove();
                
                // movieDB.movies.splice(i, 1);
                films.splice(i, 1);

                RebuildFilmsList(films, parent);
            });
        });

        // var newFilm = {
        //     isFilmFavourite: false,
        //     name: "",
        //     filmNameMaxSize: 21,
        //     setIsFavourite: function () {
        //         this.isFilmFavourite = !this.isFilmFavourite;
        //         //console.log(`isFilmFavourite pressed. Current state: ${this.isFilmFavourite}`);
        //     },
        //     setName: function (name) {
        //         if (name) {
        //             if (name.length > this.filmNameMaxSize) {
        //                 this.name = name.substring(0, this.filmNameMaxSize) + "...";
        //             } else {
        //                 this.name = name;
        //             }
        //             console.log(`film name set to: ${this.name}`);
        //         }
        //     },
        //     reset: function () {
        //         this.isFilmFavourite = false;
        //         this.name = "";
        //     }
        // };


        // var submitForm = document.querySelector(".add");
        // console.dir(submitForm);
        // var formFilmName = submitForm[0];
        // var formFavouriteCheckbox = submitForm[1];
        // var formSubmitBtn = submitForm[2];

        //var checkBox = document.querySelector(`input[type="checkbox" i]`);
        //console.dir(checkBox);
        // formSubmitBtn.addEventListener("click", (event) => {
        //     event.preventDefault();

        //     if (newFilm.name != "") {
        //         if (newFilm.isFilmFavourite) {
        //             console.log("Добавляем любимый фильм");
        //         }

        //         movieDB.movies.push(newFilm.name);
        //         RebuildFilmsList();


        //         newFilm.reset();
        //         formFilmName.value = "";
        //         checkBox.checked = false;
        //     }
        // });
    }
});