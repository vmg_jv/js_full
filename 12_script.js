/* Задание на урок:

1) Создать переменную numberOfFilms и в неё поместить ответ от пользователя на вопрос:
'Сколько фильмов вы уже посмотрели?'

2) Создать объект personalMovieDB и в него поместить такие свойства:
    - count - сюда передается ответ на первый вопрос
    - movies - в это свойство поместить пустой объект
    - actors - тоже поместить пустой объект
    - genres - сюда поместить пустой массив
    - privat - в это свойство поместить boolean(логическое) значение false

3) Задайте пользователю по два раза вопросы:
    - 'Один из последних просмотренных фильмов?'
    - 'На сколько оцените его?'
Ответы стоит поместить в отдельные переменные
Записать ответы в объект movies в формате: 
    movies: {
        'logan': '8.1'
    }

Проверить, чтобы все работало без ошибок в консоли */

'use strict';

let numberOfFilms = null;
do{
    numberOfFilms = +prompt("Сколько фильмов вы уже посмотрели?", "");
    if(isNaN(numberOfFilms)){
        alert("Введенный ответ не цифа. Пожалуйста, повторите ввод.");
        console.log("isNaN(numberOfFilms): " + isNaN(numberOfFilms));
        numberOfFilms = null;
    }
} while (! numberOfFilms);

let personalMovieDB = {
    "count": numberOfFilms,
    "movies": {},
    "actors": {},
    "genres": [],
    "privat": false
};

let i = 0;
while(i < 2){
    let filmName = prompt("Один из последних просмотренных фильмов?", "");
    console.log("filmName: " + filmName);
    let lastWatchedFilmRank = null;
    do{
        lastWatchedFilmRank = +prompt("На сколько оцените его?", "");
        if(isNaN(lastWatchedFilmRank)){
            alert("Введенный ответ не цифа. Пожалуйста, повторите ввод.");
            console.log("isNaN(lastWatchedFilmRank): " + isNaN(lastWatchedFilmRank));
            lastWatchedFilmRank = null;
        }
    } while (! lastWatchedFilmRank);
    console.log("lastWatchedFilmRank: " + lastWatchedFilmRank);
    personalMovieDB.movies[filmName] = lastWatchedFilmRank;
    i++;
}


console.log("personalMovieDB: " + personalMovieDB);
document.write(JSON.stringify(personalMovieDB, 4));