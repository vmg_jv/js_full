/* Задание на урок:

1) Автоматизировать вопросы пользователю про фильмы при помощи цикла

2) Сделать так, чтобы пользователь не мог оставить ответ в виде пустой строки,
отменить ответ или ввести название фильма длинее, чем 50 символов. Если это происходит - 
возвращаем пользователя к вопросам опять

3) При помощи условий проверить  personalMovieDB.count, и если он меньше 10 - вывести сообщение
"Просмотрено довольно мало фильмов", если от 10 до 30 - "Вы классический зритель", а если больше - 
"Вы киноман". А если не подошло ни к одному варианту - "Произошла ошибка"

4) Потренироваться и переписать цикл еще двумя способами*/

'use strict';

// Код возьмите из предыдущего домашнего задания

let numberOfFilms = null;
do {
    numberOfFilms = +prompt("Сколько фильмов вы уже посмотрели?", "");
    if (isNaN(numberOfFilms)) {
        alert("Введенный ответ не цифа. Пожалуйста, повторите ввод.");
        console.log("isNaN(numberOfFilms): " + isNaN(numberOfFilms));
        numberOfFilms = null;
    }
} while (!numberOfFilms);

let personalMovieDB = {
    "count": numberOfFilms,
    "movies": {},
    "actors": {},
    "genres": [],
    "privat": false
};

let i = 0;
while (i < 2) {
    let filmName = prompt("Один из последних просмотренных фильмов?", "");
    while (filmName === null || filmName.trim() == "" || filmName.trim().length > 50) {
        let message = "";
        if (filmName === null) {
            message = "Название фильма обязательно для заполнения.";
        } else if (filmName.trim() == "") {
            message = "Название фильма не может быть пустой строкой.";
        } else {
            message = "Название фильма не может быть длинне 50 символов.";
        }
        filmName = prompt(`${message} Пожалуйста, введите название одного из последних просмотренных фильмов?`, "");
    }
    console.log("filmName: " + filmName);
    let lastWatchedFilmRank = null;
    do {
        lastWatchedFilmRank = +prompt("На сколько оцените его?", "");
        if (isNaN(lastWatchedFilmRank)) {
            alert("Введенный ответ не цифа. Пожалуйста, повторите ввод.");
            console.log("isNaN(lastWatchedFilmRank): " + isNaN(lastWatchedFilmRank));
            lastWatchedFilmRank = null;
        }
    } while (!lastWatchedFilmRank);
    console.log("lastWatchedFilmRank: " + lastWatchedFilmRank);
    personalMovieDB.movies[filmName] = lastWatchedFilmRank;
    i++;
}

let message = "";
switch (personalMovieDB.count) {
    case ((personalMovieDB.count > 0 && personalMovieDB.count < 10) ? personalMovieDB.count : !personalMovieDB.count ): 
        message = "Просмотрено довольно мало фильмов";
        alert(message);
        console.log(message);
        break;
    case ((personalMovieDB.count > 9 && personalMovieDB.count < 30) ? personalMovieDB.count : !personalMovieDB.count ):
        message = "Вы классический зритель";
        alert(message);
        console.log(message);
        break;
    case ((personalMovieDB.count > 29) ? personalMovieDB.count : !personalMovieDB.count ):
        message = "Вы киноман";
        alert(message);
        console.log(message);
        break;
    default:
        message = "Произошла ошибка";
        alert(message);
        console.log(message);
        break;
};

console.log("personalMovieDB: " + personalMovieDB);
document.write(JSON.stringify(personalMovieDB, 4));