let money, time;
let date = new Date();
//let currentDate = "" + date.getFullYear() + "-" + date.getUTCMonth() + "-" + date.getUTCDay();
let currentDate = date.toISOString().slice(0,10);

money = +prompt("Ваш бюджет на месяц?", "0.00");
time = prompt("Введите дату в формате \"YYYY-MM-DD\"", currentDate);

var appData = {
    "bubget": money,
    "timeData": time,
    "expenses": [
        //prompt("Введите обязательную статью расходов в этом месяце", ""): prompt("Во сколько обойдется?", "")
        //"question2": null
    ],
    "optionalExpenses":{},
    "income": [],
    "savings": false

};


for ( let i = 0; i < 2; ) {
    alert("Шаг " + (i + 1) + ", из 2.");

    let tmp = prompt("Введите обязательную статью расходов в этом месяце", "");
    let tmp2 = prompt("Во сколько обойдется?", "");

    if ( tmp2 === null || tmp2 === "" || isNaN(+tmp2) ) {
        alert("Введенное значение суммы покупки - не является числом! Попробуйте еще раз.");

        continue;
    } else {
        appData.expenses[(tmp == undefined || tmp == null || tmp == "" ? i : tmp)] = tmp2;
        i += 1;
    }
}

appData.expenseSum = 0;
appData.moneyPerDay = 0;

for ( let i = 0; i < appData.expenses.length; i++ ) {
    appData.expenseSum += appData.expenses[i];
}

appData.moneyPerDay = (appData.bubget - appData.expenseSum) / 30;

alert("Ваш бюджет на 1 день: " + appData.moneyPerDay);

if ( appData.moneyPerDay < 100 ) {
    console.log("Минимальный уровень достатка.");
} else if ( appData.moneyPerDay <= 2000 ) {
    console.log("Средний уровень достатка.");
} else if ( appData.moneyPerDay > 2000 ) {
    console.log("Высокий уровень достатка.");
} else {
    console.log("Произошла ошибка.");
}

//console.log("money == " + money);
//console.log("time == " + time);
