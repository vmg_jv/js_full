/* Задания на урок:

1) Удалить все рекламные блоки со страницы (правая часть сайта)

2) Изменить жанр фильма, поменять "комедия" на "драма"

3) Изменить задний фон постера с фильмом на изображение "bg.jpg". Оно лежит в папке img.
Реализовать только при помощи JS

4) Список фильмов на странице сформировать на основании данных из этого JS файла.
Отсортировать их по алфавиту 

5) Добавить нумерацию выведенных фильмов */

'use strict';

const movieDB = {
    movies: [
        "Логан",
        "Лига справедливости",
        "Ла-ла лэнд",
        "Одержимость",
        "Скотт Пилигрим против..."
    ]
};

//1
// var advertisments = document.getElementsByClassName("promo__adv");
var advertisments = document.querySelectorAll(".promo__adv img");
// console.dir(advertisments);
//.promo__adv img
for(let i =0; i < advertisments.length; i++){
    advertisments[i].remove();
}

// //2
// let newPromoText = document.createTextNode("ДРАМА");
// var promo_genre = document.getElementsByClassName("promo__genre")[0].replaceWith(newPromoText);

let newPromoText = "ДРАМА";
//var promo_genre = document.querySelectorAll(".promo__genre")[0].textContent = newPromoText;
// promo_genre.textContent = newPromoText;
// //console.dir(;promo_genre)
var promo_genre = document.querySelector(".promo__genre").textContent = newPromoText;
// console.dir(document.querySelectorAll(".promo__genre"));


//3
let background = document.querySelector(".promo__bg");
//console.dir(background); 
background.style.backgroundImage = 'url(\'./img/bg.jpg\')';

// 4
// movieDB.movies.sort();
// let movieDbMoviesCounter = 0;
// var filmsList = document.getElementsByClassName("promo__interactive-list");
// for(let i = 0; i < filmsList.length; i++){
//     filmsList[i].replaceWith(movieDB.movies[movieDbMoviesCounter++]);
// }

// while(movieDbMoviesCounter < movieDB.movies.length){
//     filmsList.
// }

// movieDB.movies.sort();
// movieDB.movies.forEach(item => filmsList.append(item));

//filmsList.append();

// 4
movieDB.movies.sort();
var filmsList = document.querySelector(".promo__interactive-list");
//console.dir(filmsList);
filmsList.innerHTML = "";
movieDB.movies.forEach((item, i) => {
    filmsList.innerHTML += `
        <li class="promo__interactive-item">${i + 1} ${item}
            <div class="delete"></div>
        </li>
    `;
});