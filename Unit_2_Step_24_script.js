/* Задание на урок:

1) У нас уже есть рабочее приложение, состоящее из отдельных функций. Представьте, что
перед вами стоит задача переписать его так, чтобы все функции стали методами объекта personalMovieDB
Такое случается в реальных продуктах при смене технологий или подхода к архитектуре программы

2) Создать метод toggleVisibleMyDB, который при вызове будет проверять свойство privat. Если оно false - он
переключает его в true, если true - переключает в false. Протестировать вместе с showMyDB.

3) В методе writeYourGenres запретить пользователю нажать кнопку "отмена" или оставлять пустую строку. 
Если он это сделал - возвращать его к этому же вопросу. После того, как все жанры введены - 
при помощи метода forEach вывести в консоль сообщения в таком виде:
"Любимый жанр #(номер по порядку, начиная с 1) - это (название из массива)"*/

'use strict';

// Код возьмите из предыдущего домашнего задания


let personalMovieDB = {
    "count": 0,
    "movies": {},
    "actors": {},
    "genres": [],
    "privat": false,
    "SetCount": function (number) {
        this.count = number;
    },
    "ToggleVisibleMyDB": function () {
        this.privat = !this.privat;
    },
    "InputCount": function () {
        let numberOfFilms = null;
        do {
            numberOfFilms = prompt("Сколько фильмов вы уже посмотрели?", "");
            if (numberOfFilms === null) {
                this.PrintMessage("Question was Canceled", "Этот вопрос нельзя пропускать.");
            } else if (isNaN(numberOfFilms)) {
                this.PrintMessage(`was inputed: ${numberOfFilms}`, "Введенный ответ не цифа. Пожалуйста, повторите ввод.");
                numberOfFilms = null;
            }
        } while (!numberOfFilms);
        this.SetCount(numberOfFilms);
    },
    "SetMovieRank": function (movieName, movieRank) {
        this.movies[movieName] = movieRank;
    },
    "SetGenre": function (position, genre) {
        this.genres[position] = genre;
    },
    "GetGenre": function (position) {
        if (position < this.genres.length) {
            return this.genres[position];
        } else {
            return "Error. Position is bigger than genres.length";
        }
    },
    "InputMovieAndRank": function () {
        let filmName = prompt("Один из последних просмотренных фильмов?", "");
        while (filmName === null || filmName.trim() == "" || filmName.trim().length > 50) {
            let message = "";
            if (filmName === null) {
                message = "Название фильма обязательно для заполнения.";
            } else if (filmName.trim() == "") {
                message = "Название фильма не может быть пустой строкой.";
            } else {
                message = "Название фильма не может быть длинне 50 символов.";
            }
            filmName = prompt(`${message} Пожалуйста, введите название одного из последних просмотренных фильмов?`, "");
        }
        this.PrintMessage("filmName: " + filmName);
        let lastWatchedFilmRank = null;
        do {
            lastWatchedFilmRank = prompt("На сколько оцените его?", "");
            if (lastWatchedFilmRank === null) {
                this.PrintMessage("Question was Canceled", "Этот вопрос нельзя пропускать.");
            } else if (isNaN(lastWatchedFilmRank)) {
                this.PrintMessage(`isNaN(lastWatchedFilmRank): ${isNaN(lastWatchedFilmRank)}`, "Введенный ответ не цифа. Пожалуйста, повторите ввод.");
                lastWatchedFilmRank = null;
            }
        } while (!lastWatchedFilmRank);
        this.PrintMessage(`lastWatchedFilmRank: ${lastWatchedFilmRank}`);
        this.SetMovieRank(filmName, +lastWatchedFilmRank);
    },
    "GetUserRank": function () {
        let message = "";
        switch (this.count) {
            case ((this.count > 0 && this.count < 10) ? this.count : !this.count):
                message = "Просмотрено довольно мало фильмов";
                this.PrintMessage(message, message);
                break;
            case ((this.count > 9 && this.count < 30) ? this.count : !this.count):
                message = "Вы классический зритель";
                this.PrintMessage(message, message);
                break;
            case ((this.count > 29) ? this.count : !this.count):
                message = "Вы киноман";
                this.PrintMessage(message, message);
                break;
            default:
                message = "Произошла ошибка";
                this.PrintMessage(message, message);
                break;
        }
    },
    "WriteYourFavouriteGenres": function () {
        for (let i = 1; i < 4; i++) {
            this.SetGenre(i - 1, this.WriteYourFavouriteGenreN(i));
        }

        this.PrintGenres();
    },
    "WriteYourFavouriteGenreN": function (position) {
        let genre = null;
        do {
            genre = prompt(`Ваш любимый жанр под номером ${position}`, "");
            if (genre == null) {
                let message = "Жанр - обязателен к заполнению.";
                this.PrintMessage(message, message);
            } else if (genre.trim() == "") {
                let message = "Жанр - не может быть пустой строкой.";
                this.PrintMessage(message, message);
                genre = null;
            }
        } while (!genre);

        return genre;
    },
    ShowMyDB: function () {
        if (!this.privat) {
            this.PrintMessage(this);
        } else {
            this.PrintMessage(`DB is private!`);
        }
    },
    "PrintGenres": function () {
        this.genres.forEach((value, index, genres) => {
            this.PrintMessage(`Любимый жанр #${index + 1} - это ${value}`);
        });
    },
    "PrintMessage": function (messageConsole = null, messageAlert = null) {
        if (messageConsole != undefined) {
            console.log(messageConsole);
        }
        if (messageAlert != undefined) {
            alert(messageAlert);
        }
    }
};



// console.log(personalMovieDB.privat);
personalMovieDB.ShowMyDB();
personalMovieDB.ToggleVisibleMyDB();
// console.log(personalMovieDB.privat);
personalMovieDB.ShowMyDB();

personalMovieDB.WriteYourFavouriteGenres();

